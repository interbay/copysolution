# README - CopySolution v1.0.6 #
April Drop

### What is this repository for? ###

* Powershell script for copying a Visual Studio LightSwitch solution
* Version: 1.0.6

### Whats new? ###

* Logic that allows the solution to copy to be named lsWires and not change the names of the core files
* Logic to make IIS Express not randomly assign ports.  

### How do I get set up? ###

* Pretty simple, easiest is to drop the script into the root of your solution you want to copy.  
* Where the sln file is located.
* Open a cmd prompt, navigate to the folder, type in .\CopySolution.ps1
* Your Powershell should be configured to allow unsigned scripts to execute
* No dependencies
* Go to http://lightswitch.codewriting.tips for a tutorial